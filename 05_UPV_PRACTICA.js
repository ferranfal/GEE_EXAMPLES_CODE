// Crea una geometria y llamala geometry de tu zona de estudio
// Revisa los parametros de visualizaci´ón de los Map.AddLayer..

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                  TIPOS DE DATOS
////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Inicio de los principales tipos de datos que tenemos en GEE

// Hello world
print('Hello world')

// Un numero
var number = 1

// Una cadena
var string = 'Helo, world'

// Una lista
var list = [1.23, 8, -3]

//Un dicionario
var dictionary = {
  a:'hello',
  b: 10, 
  c: 0.1343,
  d: list[1]
}

// impresiones de todo
print(list[2]);
print(dictionary.d);// para sacar la variable b del diccionario
print(number, string, list, dictionary); // imprime or consola cada variable. Entero, string, lista, diccionario


////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                  NDVI
////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Indice de vegetacion NDVI con Sentinel 2

var s2 = ee.ImageCollection("COPERNICUS/S2_SR_HARMONIZED")
            .filterBounds(geometry)
            .filterDate('2024-11-01', '2024-12-01')
            .filter('CLOUDY_PIXEL_PERCENTAGE < 50')
            .median()
            .clip(geometry)
            

print('s2', s2)
Map.addLayer(s2, vs, 'Sentinel2')


// Calcula el NDVI: (NIR - RED) / (NIR + RED)
var ndvi = s2.normalizedDifference(['B8', 'B4']).rename('NDVI')

print('ndvi', ndvi)

Map.addLayer(ndvi, vsndvi, 'NDVI');


////////////////////////////////////////////////////////////////////////////////////////////////////////////
//               ERA5
////////////////////////////////////////////////////////////////////////////////////////////////////////////

//ERA5-Land es un conjunto de datos de reanálisis que proporciona una visión coherente de la
// evolución de las variables terrestres a lo largo de varias décadas con una resolución
// mejorada en comparación con ERA5. ERA5-Land se ha producido reproduciendo el componente terr
//estre del reanálisis climático ERA5 del ECMWF%


// Cargar la colección de imágenes de ERA5-Land
var era5 = ee.ImageCollection('ECMWF/ERA5_LAND/DAILY_AGGR')
                .select('temperature_2m')
                .filterDate('2022-01-01', '2024-01-01') // Ajustar fechas a los últimos 2 años
                .filterBounds(geometry);
print('Dataset filtrado:', era5);

// Visualización en el mapa (opcional)
Map.setCenter(70, 45, 3); // Ajusta la vista del mapa
Map.addLayer(era5.mean().clip(geometry), {min: 250, max: 320, palette: ['blue', 'green', 'red']}, 'Temperatura ERA5');

// Opciones del gráfico
var optionsgraf = {
  title: 'Temperatura diaria (K) a 2m de altura',
  hAxis: { title: 'Fecha' },
  vAxis: { title: 'Temperatura (K)' },
  lineWidth: 3,
  pointSize: 3,
  series: {
    0: { color: 'FF0000' }
  }
};

// Crear el gráfico de serie temporal
var timeSeriesChart = ui.Chart.image.seriesByRegion({
    imageCollection: era5,
    regions: geometry, // La región (en este caso, un punto)
    reducer: ee.Reducer.mean()
  })
  .setOptions(optionsgraf); // Incorporar los parámetros del gráfico

// Mostrar el gráfico en la consola
print(timeSeriesChart);


////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                  LST
////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Land Surface Temperarture. Temperatura de la superficie terrestre.
// LST con Landsat: Resolucion de 100 m y 16 dias
// LST con Modis: Resolucion de 1000 m y 1 dias


var time_start = '2023-01-01'
var time_end = '2024-12-31'

// Coleccion de landsat LST
var landsat_base = ee.ImageCollection("LANDSAT/LC08/C02/T1_L2")
                .filterDate(time_start, time_end)
                .filterBounds(geometry)
                .filter(ee.Filter.lt('CLOUD_COVER', 10))
                
var landsat_lst = landsat_base.map(function(img){
                                  var tir = img.select('ST_B10')
                                  var tir_gain = ee.Number(img.get('TEMPERATURE_MULT_BAND_ST_B10'))
                                  var tir_offset = ee.Number(img.get('TEMPERATURE_ADD_BAND_ST_B10'))
                                  var lst = tir.multiply(tir_gain).add(tir_offset)
                                  var lst_clip = lst.clip(geometry)
                                  return lst_clip.rename('landsat_lst')
                                  .copyProperties(img, ['system:time_start','system:index'])});

print('landsat_lst', landsat_lst)
Map.addLayer(landsat_lst,{}, 'landsat_lst')

// Coleccion Modis LST
var modis_lst = ee.ImageCollection("MODIS/061/MOD11A1")
                .filterDate(time_start, time_end)
                .map(function(img){
                  var lst = img.select('LST_Day_1km').multiply(0.02)
                  var lst_clip = lst.clip(geometry)
                  return lst_clip.rename('modis')
                  .copyProperties(img, ['system:time_start', 'system:index'])
                  })

print('modis_lst', modis_lst)
Map.addLayer(modis_lst, {}, 'modis_lst')



//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                   LST DOWNSCALING
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

// // Hacer Downscaling entre Landsat y Modis, de modo que tengamos LST a 100 m y 1 dia. Lo mejor de las 2 partes

/*
Tutorial Code by Amirhossein Ahrari
YouTube: https://www.youtube.com/@amirhosseinahrarigee

This code is part of a tutorial series on Earth Engine programming techniques
presented on the Amirhossein Ahrari YouTube channel. You are free to use and modify
this code for academic and non-academic purposes. Don't forget to subscribe to
the Amirhossein Ahrari channel and follow the videos to support the instructor!
*/
 
// print(geometry)
// Map.centerObject(geometry)

// var time_start = '2023-01-01', time_end = '2024-12-31'

// // Coleccion de landsat
// var landsat_base = ee.ImageCollection("LANDSAT/LC08/C02/T1_L2")
//                 .filterDate(time_start, time_end)
//                 .filterBounds(geometry)
//                 .filter(ee.Filter.lt('CLOUD_COVER', 10))

// // Obtenemos un solo path y row de la primera imagen
// var landsat_first = landsat_base.first()
// var path = ee.Number(landsat_first.get('WRS_PATH'))
// var row = ee.Number(landsat_first.get('WRS_ROW'))


// // Filtrado de la coleccion landsat por el path y row y calculo de la temperatura con los coeficientes 
// var landsat = landsat_base.filter(ee.Filter.and(
//                                           ee.Filter.eq('WRS_PATH', path),
//                                           ee.Filter.eq('WRS_ROW', row)))
//                           .map(function(img){
//                                   var tir = img.select('ST_B10')
//                                   var tir_gain = ee.Number(img.get('TEMPERATURE_MULT_BAND_ST_B10'))
//                                   var tir_offset = ee.Number(img.get('TEMPERATURE_ADD_BAND_ST_B10'))
//                                   var lst = tir.multiply(tir_gain).add(tir_offset)
//                                   return lst.rename('landsat')
//                                   .copyProperties(img, ['system:time_start','system:index'])
//                   });


// var modis = ee.ImageCollection("MODIS/061/MOD11A1")
//                 .filterDate(time_start, time_end)
//                 .map(function(img){
//                   var lst = img.select('LST_Day_1km').multiply(0.02)
//                   return lst.rename('modis')
//                   .copyProperties(img, ['system:time_start', 'system:index'])
//                   })

// print('landsat', landsat)

// print('modis', modis)



// // Maximo numero de dias de una imagen a otra
// var day = 2
// // Conversion a formato fecha de GEE
// var mills = ee.Number(day).multiply(1000 * 3600 * 24)

// // Union de las 2 colecciones con el filtro de los 2 dias
// var filter = ee.Filter.maxDifference({
//   difference: mills, leftField: 'system:time_start', rightField: 'system:time_start'
//   });
// var join = ee.Join.saveAll('modis_temp');

// var join_collections = ee.ImageCollection(join.apply(landsat, modis, filter))

// print('join_collections', join_collections) 

// // Pasar las imagenes de modis de properties a bands. Y tomar solo una imagen xq moddis tiene 4 por cada una de landsat
// var collection = join_collections.map(function(img){
//   var modis = ee.ImageCollection.fromImages(img.get('modis_temp')).max();
//   return img.addBands(modis)
//   })  

// print('collection', collection)

// // Creacion del modelo de regresion linear
// var model = collection.map(function(img){
//   var modis = img.select('modis')
//   var landsat = img.select('landsat')
//   var cons = ee.Image.constant(1)
//   var indep = modis.addBands(cons)
//   var dep = landsat
//   return indep.addBands(dep)
//   }).reduce(ee.Reducer.linearRegression(2, 1));
  
// print('model', model)

// // Obtenemos los coeficientes con el modelo generado anteriormente. 
// // Gain = peso, offset = Bias
// var model_gain = model.select('coefficients').arrayGet([0,0])
// var model_offset = model.select('coefficients').arrayGet([1,0])

// print('model_gain', model_gain)
// print('model_offset', model_offset)

// Map.addLayer(model_gain,{}, 'model_gain', false)

// //Aplicacion del modelo sobre los datos de modis. En la coleccion de imagenes coincidentes entre landsat y modis
// // Se generan 2 bandas, 1 modis original a 1000 m y otra modis con resolucion a 100 m
// var modis100 = collection.map(function(img){
//   var modis = img.select('modis');
//   var modis_sharp  = modis.multiply(model_gain).add(model_offset).rename('modis100')
//   return modis.addBands(modis_sharp)
//   .copyProperties(img, img.propertyNames())
//   })

// print('modis100', modis100)


// Map.addLayer(modis100.toBands().clip(geometry), vismod,'modis',false)
// print(modis100.toBands(), 'modis100')



// var lst_modis = modis.map(function(img){
//                           return img.addBands(img.multiply(model_gain).add(model_offset).rename('modis100'))
//                           })

// print('lst_modis', lst_modis)


// // Aqui Aplicamos el modelo (Los coeficientes) a una nueva serie de modis 
// // de los 31 dias del mes de Julio. Este modelo lo podemos aplicar donde queramos
// // lo mismo de antes 2 bandas:
// // 1 modis. Resolucion a 1000 m
// // 2 shapened. Resolucion a 1000 m 
// var mod_temp_100 = modis
//                     .filter(ee.Filter.calendarRange(7,7,'month'))
//                     .map(function(img){
//                           return img.addBands(img.multiply(model_gain).add(model_offset).rename('modis100'))
//                           })

// print('mod_temp_100',mod_temp_100)
// Map.addLayer(mod_temp_100.toBands().clip(geometry),vismod100,'mod100',false)
// // print(mod_temp_100.toBands(), 'mod_temp_100')

// //////////////////////////// REDUCER BY GEOMETRY ////////////////////////////////////////

// // Function to reduce region and compute median
// function computeMedian(image) {
//   var stats = image.reduceRegion({
//     reducer: ee.Reducer.median(),
//     geometry: geometry,
//     scale: 100,
//     crs: 'EPSG:4326' 
//   });

//   return image.set(stats);
// }


// var lst_reduce = lst_modis.select('modis100')
//                               .map(computeMedian)
                              
// print('lst_reduce', lst_reduce)
                              


// // /////////////////////////// LST TEMPORAL SERIES ////////////////////////////////////////


// var optionsgraf = {
//   title: 'LST',
//   hAxis:{title:'Año'},
//   vAxis:{title:'LST'},
//   lineWhidth:3
// };

// // SELECIONAMOS LA BANDA QUE HEMOS INTRODUCIDO. NDVI
// var lst_collection = lst_reduce.select('modis100');

// // CREAMOS EL GRAFICO
// var grafico_serie = ui.Chart.image.series(lst_collection, geometry)
//         .setOptions(optionsgraf);

// // MOSTRAR EL GRAFICO LA SERIE TEMPORAL
// print('GRAFICO', grafico_serie);






